#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Vehicle.Models;

    public class MvcVehicleMakeContext : DbContext
    {
        public MvcVehicleMakeContext (DbContextOptions<MvcVehicleMakeContext> options)
            : base(options)
        {
        }

        public DbSet<Vehicle.Models.VehicleMake> VehicleMake { get; set; }
    }
