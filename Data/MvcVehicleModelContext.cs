#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Vehicle.Models;

    public class MvcVehicleModelContext : DbContext
    {
        public MvcVehicleModelContext (DbContextOptions<MvcVehicleModelContext> options)
            : base(options)
        {
        }

        public DbSet<Vehicle.Models.VehicleModel> VehicleModel { get; set; }
    }
